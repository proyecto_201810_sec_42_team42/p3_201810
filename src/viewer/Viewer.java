package viewer;

import java.io.IOException;
import java.util.Scanner;

import javax.swing.plaf.synth.SynthScrollBarUI;

import com.teamdev.jxmaps.internal.internal.e;

import model.logic.project3_manager;
import model.value_objects.Camino;
import model.value_objects.KmlFormater;
import structures.data_structures.Edge;
import structures.data_structures.QueStack;
import structures.data_structures.Vertex;

public class Viewer {
	private static final Timer t = new Timer();
	private static final project3_manager manager = new project3_manager();

	public static void main(String[] args) throws IOException {
		Scanner sc = new Scanner(System.in);
		boolean fin = false;
		KmlFormater k = new KmlFormater();
		while(!fin) {	printMenu();			
		int opcion = sc.nextInt();
		switch (opcion) {
		case 0:	

			printMenuLoad();

			int optionCargar = sc.nextInt();

			String linkJson = new String();
			switch (optionCargar){
			case 1:	linkJson = manager.DIRECCION_25MTS  ;	break; //Direcci�n del archivo de datos de 25mts a la redonda
			case 2:	linkJson = manager.DIRECCION_50MTS  ;	break; //Direcci�n del archivo de datos de 50mts a la redonda
			case 3:	linkJson = manager.DIRECCION_75MTS  ;	break; //Direcci�n del archivo de datos de 75mts a la redonda
			case 4:	linkJson = manager.DIRECCION_100MTS ;	break;} //Direcci�n del archivo de datos de 100mts a la redonda
			System.out.println("Datos cargados: " + linkJson);

			t.start();	manager.loadGraph(linkJson); t.end();	break;

		case 1:	//Sebasti�n
			// Mostrar latitud, longitud, total de servicios que salieron y total de servicios que llegaron.
			System.out.println("Informaci�n del v�rtice m�s congestionado: ");
			try {
				manager.R1();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				System.out.println("Error en la escritura de KML");
			}

			break;
		case 2:	//Sebasti�n
			try {
				manager.componentesConexas2AMaps();
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				System.out.println("No es posible generar el mapa, intente nuevamente.");
			}
			System.out.println("Mapa cargado exitosamente");			
			break;
		case 3:	//Sebasti�n
			try {
				manager.componentesConexas3AMaps();
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("Error en la generaci�n del mapa");
			}
			System.out.println("Mapa cargado exitosamente");			
			break;
		case 4:	//Anderson
			System.out.println("�Quieres obtener unas coordenadas aleatorias?  S/N");

			double[] latsLongs4 = printShuffleCoords(sc.next());
			Camino caminoDeCostoMinimoReq4 = manager.pathBetween(latsLongs4[0], latsLongs4[1], latsLongs4[2], latsLongs4[3]);
			String coordenadasReq4 = "";

			if(caminoDeCostoMinimoReq4.esCamino()) {System.out.println();caminoDeCostoMinimoReq4.print();

			for (Edge e : caminoDeCostoMinimoReq4.getArcos()) {
				if(coordenadasReq4.equals(""))coordenadasReq4 = e.coordIn() + ",2357\n" + e.coordFn() + ",2357\n";
				coordenadasReq4 += e.coordFn() + ",2357\n";}
			String formated = k.format(coordenadasReq4, "Requerimiento 4: camino de menor costo");k.writeFile("requerimiento4.kml", formated);
			}else System.err.println("\nLo lamento, no hay un camino entre estos puntos :'v\n");

			break;
		case 5:	//Anderson
			System.out.println("�Quieres obtener unas coordenadas aleatorias?  S/N");
			double[] latsLongs5 = printShuffleCoords(sc.next());

			Camino[] caminosReq5 = manager.pathsBetween(latsLongs5[0], latsLongs5[1], latsLongs5[2], latsLongs5[3]);
			String coordenadas1Req5 = "", coordenadas2Req5 = "";

			if(caminosReq5[0]!= null && caminosReq5[0].esCamino()) {

				System.out.println("\t El camino de m�nimo costo desde s hasta v es: \n");
				caminosReq5[0].print();

				for (Edge e : caminosReq5[0].getArcos()) {
					if(coordenadas1Req5.equals(""))coordenadas1Req5 = e.coordIn() + ",2357\n";
					coordenadas1Req5 += e.coordFn() + ",2357\n";
				}
			}else System.out.println("\nlo lamento, no hay un camino de ida (s-v) :'v \n");
			if(caminosReq5[1]!= null && caminosReq5[1].esCamino()) {

				System.out.println("\t El camino de m�nimo costo desde v hasta s es: \n");
				caminosReq5[1].print();

				for (Edge e : caminosReq5[1].getArcos()) {
					if(coordenadas2Req5.equals(""))coordenadas2Req5 = e.coordIn()+ ",2357\n";
					coordenadas2Req5 += e.coordFn() + ",2357\n";
				}
			}else System.out.println("lo lamento, no hay un camino de regreso (v-s) :'v\n");

			KmlFormater k5 = new KmlFormater();
			String formated = k5.format(coordenadas1Req5, coordenadas2Req5, "Requerimiento 5: camino minimo ida y vuelta");
			k5.writeFile("requerimiento5.kml", formated);

			break;
		case 6:	//Anderson
			System.out.println("�Quieres obtener unas coordenadas aleatorias?  S/N");
			double[] latsLongs6 = printShuffleCoords(sc.next());

			QueStack<Camino> cs = (QueStack<Camino>) manager.patsWhitoutTollsBetween(latsLongs6[0], latsLongs6[1], latsLongs6[2], latsLongs6[3]);

			Camino toPrint1 = new Camino(); 
			Camino toPrint2 = new Camino(); 
			if(cs.isEmpty())System.out.println("No hay camino sin peajes hasta el punto dado");
			else {
				for (Camino c : cs) {toPrint1 = c;break;}
				for (Camino c : cs) {
					if(!c.esCamino())continue;
					c.printSimple();
					toPrint2 = c;
				}

				String coordenadas1Req6 = "", coordenadas2Req6 = "";
				if(toPrint1 != null && toPrint2 != null && toPrint1.esCamino() && toPrint2.esCamino()) {
					for (Edge e : toPrint1.getArcos()) {
						if(coordenadas1Req6.equals(""))coordenadas1Req6 = e.coordIn() + ",2357\n";
						coordenadas1Req6 += e.coordFn() + ",2357\n";
					}for (Edge e2 : toPrint2.getArcos()) {
						if(coordenadas2Req6.equals(""))coordenadas2Req6 = e2.coordIn() + ",2357\n";
						coordenadas2Req6 += e2.coordFn() + ",2357\n";
					}
					KmlFormater k6 = new KmlFormater();
					String formated6 = k6.format(coordenadas1Req6, coordenadas2Req6, "Requerimiento 6: dos caminos entre dos puntos");
					k6.writeFile("requerimiento6.kml", formated6);
				}
			}

			break;
		case 7:	 fin = true; sc.close(); break; //m�todo de salida
		case 8:	
			manager.printGraph();
			break;
		case 9:	
			manager.printGraphVertex();
			break;
		}}
	}

	private static void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Proyecto 3--------------------");
		System.out.println("Iniciar la Fuente de Datos a Consultar :");
		System.out.println(" \n0. Cargar toda la informacion del sistema de una fuente de datos (25, 50, 75 o 100). Y el .csv");


		System.out.println("\n Requerimientos (�tiles despues de la carga):\n");
		System.out.println("  Requerimientos hechos por sebasti�n\n");
		System.out.println(" 1. Obtener v�rtice m�s congestionado en Chicago");
		System.out.println(" 2. Obtener componentes fuertemente conexos");
		System.out.println(" 3. Generar mapa coloreado de la red vial de Chicago en Google Maps");

		System.out.println("  \nRequerimientos hechos por Anderson\n");
		System.out.println(" 4. Encontrar camino de menor distancia para dos puntos aleatorios");
		System.out.println(" 5. Hallar caminos de menor duracion entre dos puntos aleatorios,, de ida y vuelta");
		System.out.println(" 6. Encontrar caminos sin peaje entre dos puntos aleatorios");
		System.out.println(" \n7. -salida");
		System.out.println("Ingrese el numero de la opcion seleccionada y presione <Enter> para confirmar: (e.g., 1):");
	}
	private static void printMenuLoad() {
		System.out.println("-- �Que fuente de datos desea cargar?");
		System.out.println("-- 1. 25mts");
		System.out.println("-- 2. 50mts");
		System.out.println("-- 3. 75mts");
		System.out.println("-- 4. 100mts");
		System.out.println("-- Ingrese el numero de la fuente a cargar y presione <Enter> para confirmar: (e.g., 1)");
	}
	private static double[] printShuffleCoords(String print) {
		Scanner sc = new Scanner(System.in);
		if(print.equals("s") || print.equals("S")) System.out.println(manager.shuffleCoords());
		else System.out.println("coordenadas de ejemplo:\n-87.65622713045708,41.79961239014777\n-87.70422380899832,41.85816076814258\n");
		System.out.println();
		System.out.println("\t \t las latitutdes en chicago son positivas \n");
		System.out.println("Por favor introduzca la latitud y longitud del primer punto...");
		String[] latLog_1 = sc.next().split(",");
		double lat_1 = Double.parseDouble(latLog_1[1]);
		double long_1 = Double.parseDouble(latLog_1[0]);
		System.out.println("Por favor introduzca la latitud y longitud del segundo punto...");
		String[] latLog_2 = sc.next().split(",");
		double lat_2 = Double.parseDouble(latLog_2[1]);
		double long_2 = Double.parseDouble(latLog_2[0]);
		double[] toRet = new double[4]; toRet[0] = lat_1;toRet[1] = long_1;toRet[2] = lat_2;toRet[3] = long_2;
		return toRet;
	}
}
