package model.value_objects;

import structures.data_structures.DigrafoConPeso;
import structures.data_structures.Edge;
import structures.data_structures.QueStack;
import structures.data_structures.Stack;
import structures.data_structures.Vertex;

/**
 *  @author Robert Sedgewick
 *  @author Kevin Wayne
 */
public class DepthFirstOrder {
	private boolean[] marked;          // marked[v] = has v been marked in dfs?
	private int[] pre;                 // pre[v]    = preorder  number of v
	private int[] post;                // post[v]   = postorder number of v
	private QueStack<Integer> preorder;   // vertices in preorder
	private QueStack<Integer> postorder;  // vertices in postorder
	private int preCounter;            // counter or preorder numbering
	private int postCounter;           // counter for postorder numbering
	private Vertex[] anderson;

	/**
	 * Determines a depth-first order for the edge-weighted digraph {@code G}.
	 * @param G the edge-weighted digraph
	 */
	public DepthFirstOrder(DigrafoConPeso G) {
		anderson = G.indexedArray();
		pre    = new int[G.vertices()];
		post   = new int[G.vertices()];
		postorder = new QueStack<Integer>();
		preorder  = new QueStack<Integer>();
		marked    = new boolean[G.vertices()];
		for (int v = 0; v < G.vertices(); v++)
			if (!marked[v]) dfs(G, v);
		
	}


	// run DFS in edge-weighted digraph G from vertex v and compute preorder/postorder
	private void dfs(DigrafoConPeso G, int v) {
		marked[v] = true;
		pre[v] = preCounter++;
		preorder.enqueue(v);
		for (Edge w : anderson[v].edges()) {
			if (!marked[w.ladoTo()]) {
				dfs(G, w.ladoTo());
			}
		}
		postorder.enqueue(v);
		post[v] = postCounter++;
	}

	/**
	 * Returns the preorder number of vertex {@code v}.
	 * @param  v the vertex
	 * @return the preorder number of vertex {@code v}
	 * @throws IllegalArgumentException unless {@code 0 <= v < V}
	 */
	public int pre(int v) {
		return pre[v];
	}

	/**
	 * Returns the postorder number of vertex {@code v}.
	 * @param  v the vertex
	 * @return the postorder number of vertex {@code v}
	 * @throws IllegalArgumentException unless {@code 0 <= v < V}
	 */
	public int post(int v) {
		return post[v];
	}

	/**
	 * Returns the vertices in postorder.
	 * @return the vertices in postorder, as an iterable of vertices
	 */
	public Iterable<Integer> post() {
		return postorder;
	}

	/**
	 * Returns the vertices in preorder.
	 * @return the vertices in preorder, as an iterable of vertices
	 */
	public Iterable<Integer> pre() {
		return preorder;
	}

	/**
	 * Returns the vertices in reverse postorder.
	 * @return the vertices in reverse postorder, as an iterable of vertices
	 */
	public Iterable<Integer> reversePost() {
		Stack<Integer> reverse = new Stack<Integer>();
		for (int v : postorder)
			reverse.push(v);
		return reverse;
	}
}
