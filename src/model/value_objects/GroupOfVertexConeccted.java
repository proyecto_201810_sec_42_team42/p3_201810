package model.value_objects;

import java.awt.Color;

import structures.data_structures.LinkedList;
import structures.data_structures.Vertex;


public class GroupOfVertexConeccted {

	
	private int id;
	private Color color;
	private LinkedList<Vertex> vertices;
	private String colorString;
	
	public GroupOfVertexConeccted(int id, String pColor) {
		// TODO Auto-generated constructor stub
		colorString = pColor;
		this.id = id;
		int a = (int) (Math.random()*250);
		int b = (int) (Math.random()*250);
		int c = (int) (Math.random()*250);

//		this.color = new Color(id*10, id*10, id*10);
		vertices = new  LinkedList<>();
		this.color = new Color(a , b ,c );
	}
	
	public void setColor(String pColor){
		this.colorString = pColor;
	}
	
	public String getColor(){
		return this.colorString;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}



	public void setColor(Color color) {
		this.color = color;
	}

	public LinkedList<Vertex> getVertices() {
		return vertices;
	}

	public void addVertex(Vertex vertice) {
		this.vertices.add(vertice);
	}
	
	public String getHexColor(){
		String hex = String.format("#%02x%02x%02x", color.getRed(), color.getGreen(), color.getBlue());  
		return hex;
	}
	
	public int getCantidadVertices(){
		return vertices.size();
	}
	
	public LinkedList<Vertex> getListaVertices(){
		return vertices;
	}
	
}
