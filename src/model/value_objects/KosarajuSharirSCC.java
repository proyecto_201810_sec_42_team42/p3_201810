package model.value_objects;

import structures.data_structures.DigrafoConPeso;
import structures.data_structures.Edge;
import structures.data_structures.Vertex;

/**
 *  @author Robert Sedgewick
 *  @author Kevin Wayne
 */
public class KosarajuSharirSCC {
	private boolean[] marked;     // marked[v] = has vertex v been visited?
	private int[] id;             // id[v] = id of strong component containing v
	private int count;            // number of strongly-connected components
	private Vertex[] anderson;

	/**
	 * Computes the strong components of the digraph {@code G}.
	 * @param G the digraph
	 */
	public KosarajuSharirSCC(DigrafoConPeso G) {

		// compute reverse postorder of reverse graph
		DepthFirstOrder dfs = new DepthFirstOrder(G.reverse());

		// run DFS on G, using reverse postorder to guide calculation
		marked = new boolean[G.vertices()];
		id = new int[G.vertices()];
		anderson = G.indexedArray();
		for (int v : dfs.reversePost()) {
			if (!marked[v]) {
				dfs(G, v);
				count++;
			}
		}

		// check that id[] gives strong components
		//		assert check(G);
	}

	// DFS on graph G
	private void dfs(DigrafoConPeso G, int v) { 
		marked[v] = true;
		id[v] = count;
		for (Edge w : anderson[v].edges()) {
			if (!marked[w.ladoTo()]) dfs(G, w.ladoTo());
		}
	}

	/**
	 * Returns the number of strong components.
	 * @return the number of strong components
	 */
	public int count() {
		return count;
	}

	/**
	 * Are vertices {@code v} and {@code w} in the same strong component?
	 * @param  v one vertex
	 * @param  w the other vertex
	 * @return {@code true} if vertices {@code v} and {@code w} are in the same
	 *         strong component, and {@code false} otherwise
	 * @throws IllegalArgumentException unless {@code 0 <= v < V}
	 * @throws IllegalArgumentException unless {@code 0 <= w < V}
	 */
	public boolean stronglyConnected(int v, int w) {
		validateVertex(v);
		validateVertex(w);
		return id[v] == id[w];
	}

	/**
	 * Returns the component id of the strong component containing vertex {@code v}.
	 * @param  v the vertex
	 * @return the component id of the strong component containing vertex {@code v}
	 * @throws IllegalArgumentException unless {@code 0 <= s < V}
	 */
	public int id(int v) {
		validateVertex(v);
		return id[v];
	}

	/**
	 * 
	 * @param G
	 * @return
	 */
	public int[] getStronglyConectedIds(){
		return id;
	}

	// does the id[] array contain the strongly connected components?
	private boolean check(DigrafoConPeso G) {
		TransitiveClosure tc = new TransitiveClosure(G);
		for (int v = 0; v < G.vertices(); v++) {
			for (int w = 0; w < G.vertices(); w++) {
				if (stronglyConnected(v, w) != (tc.reachable(v, w) && tc.reachable(w, v)))
					return false;
			}
		}
		return true;
	}

	public void printConectedComponents(){
		System.out.println("el tamanio de los id's es " + id.length);
		for (int ids : id) {
			System.out.println(ids);
		}
	}

	public int getMostStronglyConected(){
		Integer MasConectado=null;
		int masOcurrencias=0;
		int ocurrenciasTemp = 0;
		for (int first : id) {
			for (int second : id) {
				if(second==first)
					ocurrenciasTemp++;
			}
			if(ocurrenciasTemp>masOcurrencias)
				MasConectado = first;
		}
		return MasConectado;
	}


	// throw an IllegalArgumentException unless {@code 0 <= v < V}
	private void validateVertex(int v) {
		int V = marked.length;
		if (v < 0 || v >= V)
			throw new IllegalArgumentException("vertex " + v + " is not between 0 and " + (V-1));
	}

}
