package model.value_objects;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class KmlFormater {

	public static final String ENCABEZADO1 = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
			+ "<kml xmlns=\"http://www.opengis.net/kml/2.2\">\n<Document>\n<name>";
	public static final String ENCABEZADO2 =
			"</name>\n<Style id=\"line-0288D1-6000-normal\">\n"
					+"<LineStyle>\n<color>ffd18802</color>\n<width>6</width>\n</LineStyle>\n</Style>\n<Style id=\"line-0288D1-6000-highlight\">\n<LineStyle>\n"
					+"<color>ffd18802</color>\n<width>9</width>\n</LineStyle>\n</Style>\n<StyleMap id=\"line-0288D1-6000\">\n<Pair>\n<key>normal</key>\n<styleUrl>#line-0288D1-6000-normal</styleUrl>\n"
					+"</Pair>\n<Pair>\n<key>highlight</key>\n<styleUrl>#line-0288D1-6000-highlight</styleUrl>\n</Pair>\n</StyleMap>\n<Style id=\"line-FF0000-6000-normal\">\n"
					+"<LineStyle>\n<color>ff0000ff</color>\n<width>6</width>\n</LineStyle>\n</Style>\n<Style id=\"line-FF0000-6000-highlight\">\n"
					+"<LineStyle>\n<color>ff0000ff</color>\n<width>9</width>\n</LineStyle>\n</Style>\n<StyleMap id=\"line-FF0000-6000\">\n<Pair>\n<key>normal</key>\n"
					+"<styleUrl>#line-FF0000-6000-normal</styleUrl>\n</Pair>\n<Pair>\n<key>highlight</key>\n<styleUrl>#line-FF0000-6000-highlight</styleUrl>\n</Pair>\n"
					+"</StyleMap>\n<Placemark>\n<name>RutaMinima</name>\n<description>ruta minima desde v hasta s</description>\n<styleUrl>#line-FF0000-6000</styleUrl>\n<LineString>\n<tessellate>1</tessellate>\n<coordinates>\n";
	public static final String BASE = "</coordinates>\n</LineString>\n</Placemark>\n<Placemark>\n<name>RutaMinima2</name>\n<description>ruta minima desde s hasta v</description>\n"
			+"<styleUrl>#line-0288D1-6000</styleUrl>\n<LineString>\n<tessellate>1</tessellate>\n<coordinates>\n";
	public static final String LAST = "</coordinates>\n</LineString>\n</Placemark>\n</Document> </kml>";


	/**
	 * crea el string para el kml con las coordenadas pasadas por par�metro
	 */
	public String format(String coordinates, String nombreRequerimiento) {
		String formatedString = ENCABEZADO1 + nombreRequerimiento + ENCABEZADO2 + coordinates + LAST;
		return formatedString;
	}

	/**
	 * crea un string (usando las coordenadas dadas por par�metro) con dos coordenadas y colores necesario para la creaci�n del kml
	 */
	public String format(String coordinates, String coordinates2, String nombreRequerimiento) {
		String formatedString = ENCABEZADO1 + nombreRequerimiento + ENCABEZADO2 + coordinates + BASE + coordinates2 + LAST;
		return formatedString;
	}

	/**
	 * escribe un kml con el nombre pasado por par�metro y el strign formateado para la adaptacion al mapa de MyMaps
	 */
	public void writeFile(String nombreDelArchivo,String toWrite) {
		File f = new File("./data/requerimientos_Anderson/"+nombreDelArchivo);
		FileWriter fw;
		try {fw = new FileWriter(f);
		BufferedWriter bw = new BufferedWriter(fw);
		PrintWriter pw = new PrintWriter(bw);
		pw.println(toWrite);
		pw.close();
		} catch (IOException e) {e.printStackTrace();}
	}
}
