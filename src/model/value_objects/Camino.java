package model.value_objects;

import structures.data_structures.Edge;
import structures.data_structures.QueStack;
import structures.data_structures.QueStack;
import structures.data_structures.Vertex;

public class Camino implements Comparable<Camino> {
	private QueStack<Edge> arcos;
	private long tiempo;
	private double distancia;
	private double valor;
	private boolean hasTolls;
	private Vertex vertexFinal;
	public Camino() {this(0,0,0);}
	public Camino(long tiempo, double distancia, double valor) {
		this.arcos = new QueStack<Edge>();
		this.tiempo = tiempo;
		this.distancia = distancia;
		this.valor = valor;
		this.hasTolls = false;
		this.vertexFinal = new Vertex();
	}
	public long getTiempo() {
		return tiempo;
	}
	public void setTiempo(long tiempo) {
		this.tiempo = tiempo;
	}
	public double getDistancia() {
		return distancia;
	}
	public void setDistancia(double distancia) {
		this.distancia = distancia;
	}
	public double getValor() {
		return valor;
	}
	public void setValor(double valor) {
		this.valor = valor;
	}
	public boolean hasTolls() {return hasTolls;}

	public QueStack<Edge> getArcos(){
		return this.arcos;
	}
	public void setVertex(Vertex newVertex) {
		this.vertexFinal = newVertex;
	}
	public Vertex getVertex() {return this.vertexFinal;}

	public void agregarVertices(Iterable<Vertex> vertices) {
		Vertex temp = null;
		QueStack<Edge> edges = new QueStack<>();
		for (Vertex v : vertices) {
			if(temp == null) {temp = v; continue;}
			Edge e = temp.getEdge(v);
			if(e != null)edges.enqueue(e);
		}
		agregarArcos(edges);
	}

	public void agregarArco(Edge arco){
		arcos.enqueue(arco);
	}

	public void unirCaminos(Camino c) {
		if(c == null || !c.esCamino())return;
		this.distancia += c.distancia; this.tiempo += c.tiempo; this.valor += c.valor; this.vertexFinal = c.vertexFinal;
		this.arcos.inverseJoin(c.arcos);
	}

	public void agregarArcos(Iterable<Edge> listaDeArcos) {
		long time = 0; double miles = 0, total = 0;
		boolean toad = true;
		if(this.arcos.isEmpty() && listaDeArcos != null) {toad = false; this.arcos = (QueStack<Edge>) listaDeArcos;}
		if(listaDeArcos != null) {
			Edge last = null;
			if(toad)this.arcos.inverseJoin((QueStack<Edge>)listaDeArcos);
			for (Edge e : listaDeArcos) {
				if(e.hasTolls())this.hasTolls = true;
				time += e.time();
				total += e.total();
				miles += e. miles();
				last = e;
			}
			this.distancia += miles; this.tiempo += time; this.valor += total;
			this.vertexFinal = (last != null)?new Vertex(last.coordFn(), last.ladoTo()):this.vertexFinal;
		}
	}

	public boolean esCamino() {return this.arcos != null && this.arcos.size() >0 && tiempo != 0 && valor != 0 && distancia != 0;}

	public void printSimple(){
		//Tiempo estimado
		System.out.println("Tiempo estimado: " + this.getTiempo());
		//Distancia estimada
		System.out.println("Distancia estimada: " + this.getDistancia());
		//Valor estimado a pagar
		System.out.println("Valor estimado: " + this.getValor()+"\n");
	}

	public void print(){
		System.out.println("Listado de v�rtices a seguir: ");
		for (Edge e : arcos) {System.out.println("\nLatitud: " + e.coordIn().split(",")[0]+". Longitud: "+e.coordIn().split(",")[1]);break;}
		for (Edge e : arcos) {
			String[] latLog = e.coordFn().split(",");
			System.out.println("Latitud: " + latLog[0]+". Longitud: "+latLog[1]);
		}

		//Tiempo estimado
		System.out.println("Tiempo estimado: " + this.getTiempo());
		//Distancia estimada
		System.out.println("Distancia estimada: " + this.getDistancia());
		//Valor estimado a pagar
		System.out.println("Valor estimado: " + this.getValor() + "\n");
	}

	/**
	 * retorno si soy menor neg, igual 0 o positivo si mayor
	 */
	@Override public int compareTo(Camino o) {
		int rta = Long.compare(this.tiempo, o.getTiempo());
		if(rta==0)
			rta = Double.compare(this.valor, o.getValor());
		return rta;
	}
	public boolean equals(Camino o) {
		return this.compareTo(o) == 0;
	}


}
