package model.logic;

import java.io.IOException;

import model.value_objects.Camino;
import structures.data_structures.Edge;
import structures.data_structures.QueStack;
import structures.data_structures.Vertex;

public interface IProject {

	/**
	 * Mostrar en la consola de texto la informaci�n del v�rtice m�s congestionado en Chicago
	 * (aquel que contiene la mayor cantidad de servicios que salen y llegan a �l): su (latitud,
	 * longitud), total servicios que salieron y total de servicios que llegaron.
	 * @return
	 * @author Sebastian  Mujica
	 * @throws Exception 
	 */
	public Vertex R1() throws Exception;

	/**
	 * Calcule los componentes fuertemente conexos presentes en el grafo. As�gnele un color
	 * a los v�rtices que componen un componente. Retorne una lista en donde en cada nodo,
	 * se tiene la informaci�n de un componente conexo (color, n�mero de v�rtices que lo
	 * componen). Muestre en la consola de texto el total de componentes identificadas y la
	 * informaci�n de la lista de componentes conexo.
	 * @return 
	 * @author Sebastian Mujica
	 */
	public void componentesConexas2AMaps() throws InterruptedException;


	/**
	 * Visualizaci�n: A partir del grafo cargado al inicio y de los componentes conectados
	 *encontrados en el punto 2, genere un mapa coloreado de la red vial de Chicago
	 *utilizando Google Maps, de la siguiente forma:
	 *A). Para cada v�rtice del grafo, calcule el radio del circulo de acuerdo a la "densidad"
	 *de servicios que salen y llegan a dicho v�rtice. Es decir que cada nodo tiene
	 *asociado un porcentaje de los servicios que salen y llegan, del total de servicios
	 *procesados. Tenga en cuenta que cada servicio se contabiliza en el v�rtice donde
	 *inicia y en el v�rtice donde termina. El tama�o del circulo a dibujar es
	 *proporcional a dicho porcentaje. El color del v�rtice es el color de la componente
	 *a la que pertenece.
	 *B). El arco debe ser del color del componente conectado al cual pertenece el v�rtice
	 *del grafo donde inicia el servicio.
	 * @author Sebastian Mujica
	 * @throws IOException
	 */
	public void componentesConexas3AMaps() throws IOException;


	/**
	 * Encontrar el camino de costo m�nimo <i>(menor distancia)</i> para un servicio que inicia en
	 * un punto (latitud, longitud) escogido aleatoriamente de la informaci�n cargada del
	 * archivo de calles <i>(StreetLines.csv)</i> y finaliza en un punto <i>(latitud, longitud)</i> escogido
	 * tambi�n de manera aleatoria del archivo de calles. Inicie el camino en el v�rtice del
	 * grafo m�s cercano a su punto de inicio y final�cela en el v�rtice m�s cercano del grafo a
	 * su punto de destino. Muestre en la consola de texto el camino a seguir, informando sus
	 * v�rtices, el tiempo estimado, la distancia estimada y el valor estimado a pagar.
	 * @param lat1 latitud del punto inicial
	 * @param long1 longitud del punto inicial
	 * @param lat2	latitud del punto final
	 * @param long2	longitud del punto final
	 * @return el camino entre los puntos
	 * @author Anderson Barrag�n Agudelo
	 */
	public Camino pathBetween(double lat1, double long1, double lat2, double long2);

	/**
	 * Dado un servicio que inicia en un punto (latitud, longitud) escogido aleatoriamente de
	 * la informaci�n cargada del archivo de calles (StreetLines.csv) y finaliza en un punto
	 * (latitud, longitud) escogido tambi�n de manera aleatoria del archivo de calles.
	 * Aproxime los puntos de inicio y fin a los v�rtices m�s cercanos en el grafo y encuentre
	 * los caminos de mayor y menor duraci�n entre dichos puntos. Muestre en la consola de
	 * texto los dos caminos, y para cada uno de ellos: sus v�rtices, el tiempo estimado, la
	 * distancia estimada y el valor estimado a pagar.
	 * @param lat1 latitud del punto inicial
	 * @param long1 longitud del punto inicial
	 * @param lat2	latitud del punto final
	 * @param long2	longitud del punto final
	 * @return un arreglo de dos posiciones con el camino m�s "corto" en la primer posici�n
	 * @author Anderson Barrag�n Agudelo
	 */
	public Camino[] pathsBetween(double lat1, double long1, double lat2, double long2);

	/**
	 * Dado un servicio que inicia en un punto (latitud, longitud) escogido aleatoriamente de
	 * la informaci�n cargada del archivo de calles (StreetLines.csv) y finaliza en un punto
	 * (latitud, longitud) escogido tambi�n de manera aleatoria del archivo de calles. Indique
	 * si existen caminos entre ambos puntos, en los que no deba pagar peaje. Si dichos
	 * caminos existen, retorne una lista con todos ellos, ordenadas de menor a mayor por
	 * tiempo y de mayor a menor por costo.
	 * @param lat1 latitud del punto inicial
	 * @param long1 longitud del punto inicial
	 * @param lat2	latitud del punto final
	 * @param long2	longitud del punto final
	 * @return un arreglo de dos posiciones con el camino m�s "corto" en la primer posici�n (ambos caminos sin peajes)
	 * @author Anderson Barrag�n Agudelo
	 */
	public Iterable<Camino> patsWhitoutTollsBetween(double lat1, double long1, double lat2, double long2);

}
