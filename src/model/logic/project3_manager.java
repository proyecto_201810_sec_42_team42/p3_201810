package model.logic;

import java.io.File;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import javax.swing.GrayFilter;

import com.teamdev.jxmaps.LatLng;
import com.teamdev.jxmaps.Map;

import model.value_objects.Camino;
import model.value_objects.Dijkstra;
import model.value_objects.GroupOfVertexConeccted;
import model.value_objects.KosarajuSharirSCC;
import structures.data_structures.DigrafoConPeso;
import structures.data_structures.Edge;
import structures.data_structures.LinkedList;
import structures.data_structures.Mapa;
import structures.data_structures.QueStack;
import structures.data_structures.RedBlackBST;
import structures.data_structures.Vertex;

public class project3_manager implements IProject{
	//__________ CONSTANTES PARA LA CARGA DE LOS ARCHIVOS_____________________
	public final String DIRECCION_25MTS		= "./data/JsonGraphs/largeGraph25-319-.json" ;
	public final String DIRECCION_50MTS 	= "./data/JsonGraphs/largeGraph50-319-.json" ;
	public final String DIRECCION_75MTS		= "./data/JsonGraphs/largeGraph75-319-.json" ;
	public final String DIRECCION_100MTS	= "./data/JsonGraphs/largeGraph100-319-.json";



	private static char comilla = '"';
	public static final String DIRECCION_XML = "<?xml version=" + comilla+ 1.0+ comilla + " "  +"encoding="+ comilla + "UTF-8"+comilla+"?>";
	public static final String DIRECION_XML_SEGUNDA = "<kml xmlns="+ comilla + "http://earth.google.com/kml/2.0" + comilla +">"; 

	// Grafo
	private DigrafoConPeso principalGraph;
	private Vertex[] graph;
	private QueStack<String> calles = new QueStack<>();

	//------------------INICIO DE LOS METODOS DE CARGA------------------------
	public void loadGraph(String ruta) {
		principalGraph = new DigrafoConPeso(ruta);
		graph = principalGraph.indexedArray();
		loadCsv();
		System.out.println("\t" +principalGraph.vertices() + " Esta deber�a ser la cantidad de vertices");
	}

	public Vertex masCongestionado(){
		Vertex MasCongestionado = null;
		int grado = 0;
		Iterable<Vertex> vertices = principalGraph.allVertices();
		for (Vertex vertex : vertices) {
			if(vertex.degree()>grado)
			{
				MasCongestionado = vertex;
				grado = MasCongestionado.degree();
			}
		}
		return MasCongestionado;
	}

	@Override public Vertex R1() throws IOException {
		Vertex MasCongestionado = null;
		int grado = 0;
		Iterable<Vertex> vertices = principalGraph.allVertices();
		for (Vertex vertex : vertices) {
			if(vertex.degree()>grado)
			{
				MasCongestionado = vertex;
				grado = MasCongestionado.degree();
			}
		}
		String latLon[] = MasCongestionado.info().split(";;;");
		if(MasCongestionado!=null){
			FileWriter fw = new FileWriter(new File("./data/requerimientos_Sebastian/verticeMasCongestionado1A"+".kml"));
			PrintWriter pw = new PrintWriter(fw);
			pw.println(DIRECCION_XML);
			pw.println(DIRECION_XML_SEGUNDA);
			pw.println("<Document>");
			pw.println("<Placemark>");
			pw.println( "<name> Vertice mas congestionado </name>");
			pw.println("<description>"+ "Total servicios que salieron: " + MasCongestionado.degree() + ". Total servicios que llegaron: "+MasCongestionado.edgesInSize() + "</description>");
			pw.println("<Point>");
			pw.println("<coordinates>"+ latLon[1] + ","+ latLon[0] + "," + 0 + "," + "</coordinates>");
			pw.println("</Point>");
			pw.println("</Placemark> ");
			pw.println("</Document>");
			pw.println("</kml>");
			pw.close();
			fw.close();
			System.out.println("------------------------------------------------------------------------------");
			MasCongestionado.print();
			System.out.println("------------------------------------------------------------------------------");
		}
		return MasCongestionado;
	}

	public void componentesConexas2A() throws IOException {
		KosarajuSharirSCC kosaraju = new KosarajuSharirSCC(principalGraph);
		GroupOfVertexConeccted[] group = new GroupOfVertexConeccted[kosaraju.count()];
		for (int i = 0; i < group.length; i++) {
			group[i] = new GroupOfVertexConeccted(i, "#870C00");}

		int[] componentes = kosaraju.getStronglyConectedIds();
		Vertex[] vertices = principalGraph.indexedArray();
		for (int i = 0; i < vertices.length; i++) {
			Vertex temp = vertices[i];
			int index = componentes[i];
			group[index].addVertex(temp);}
		////// IMPRIMIR KML/////////////////////////////////////////
		GroupOfVertexConeccted grupoMasConectado = group[kosaraju.getMostStronglyConected()];
		FileWriter fw = new FileWriter(new File("./data/requerimientos_Sebastian/componenteConectada2Avertices"+".kml"));
		PrintWriter pw = new PrintWriter(fw);
		pw.println(DIRECCION_XML);
		pw.println(DIRECION_XML_SEGUNDA);
		pw.println("<Document>");
		LinkedList<Vertex> listaVertices = grupoMasConectado.getVertices();
		for (int i = 0; i < listaVertices.size(); i++) {
			Vertex actual = listaVertices.get(i);
			String latLon[] = actual.info().split(";;;");
			pw.println("<Placemark>");
			pw.println("<Point>");
			pw.println("<coordinates>"+ latLon[1] + ","+ latLon[0] + "," + 0 + "," + "</coordinates>");
			pw.println("</Point>");
			pw.println("</Placemark>");
		}
		pw.println("</Document>");
		pw.println("</kml>");
		pw.close();
		fw.close();
		FileWriter fw2 = new FileWriter(new File("./data/requerimientos_Sebastian/componenteConectada2Alineas"+".kml"));
		PrintWriter pw2 = new PrintWriter(fw2);
		pw2.println(DIRECCION_XML);
		pw2.println(DIRECION_XML_SEGUNDA);
		pw2.println("<Document>");
		int contador = 0;
		for (int i = 0; i < listaVertices.size(); i++) {
			Vertex actual = listaVertices.get(i);
			String latLonActual[] = actual.info().split(";;;");
			for(Edge edge : actual.edges()){
				if(kosaraju.stronglyConnected(actual.index(), edge.ladoTo())){
					contador++;
					String latLonSecond[] = edge.to().split(";;;");
					pw2.println("<Placemark>");
					pw2.println("<LineString>");
					pw2.println("<coordinates>");
					pw2.println(latLonActual[1] + ","+ latLonActual[0] + "," + 0 + ".");
					pw2.println(latLonSecond[1] + ","+ latLonSecond[0] + "," + 0 + ".");
					pw2.println("</coordinates>");
					pw2.println("</LineString>");
					pw2.println("</Placemark>");
				}
			}
		}
		pw2.println("</Document>");
		pw2.println("</kml>");
		pw2.close();
		fw2.close();
		System.out.println("la cantidad de arcos de la componente conexa m�s grande es " + contador);
	}


	public void componentesConexas2AMaps() throws InterruptedException{

		KosarajuSharirSCC kosaraju = new KosarajuSharirSCC(principalGraph);
		GroupOfVertexConeccted[] group = new GroupOfVertexConeccted[kosaraju.count()];
		for (int i = 0; i < group.length; i++) {
			group[i] = new GroupOfVertexConeccted(i, "#870C00");}

		int[] componentes = kosaraju.getStronglyConectedIds();
		Vertex[] vertices = principalGraph.indexedArray();
		for (int i = 0; i < vertices.length; i++) {
			Vertex temp = vertices[i];
			int index = componentes[i];
			group[index].addVertex(temp);
		}
		GroupOfVertexConeccted grupoMasConectado = group[kosaraju.getMostStronglyConected()];
		LinkedList<Vertex> listaVertices = grupoMasConectado.getVertices();
		System.out.println("La cantidad de componentes conexas es " + kosaraju.count());
		for (GroupOfVertexConeccted actual : group) {
			System.out.println("Componente conectada de color " + actual.getHexColor() + " La cantidad de vertices es " + actual.getCantidadVertices() );}
		Mapa mapa = new Mapa("REQUERIMIENTO 2A");
		TimeUnit.SECONDS.sleep(2);

		int contador = 0;
		LinkedList<Vertex> listaVertices1 = grupoMasConectado.getListaVertices();
		for (int i = 0; i < listaVertices1.size(); i++) {
			Vertex actual = listaVertices1.get(i);
			String latLonActual[] = actual.info().split(";;;");
			double latActual = Double.parseDouble(latLonActual[0]);
			double lonActual = Double.parseDouble(latLonActual[1]);
			mapa.generateArea(new LatLng(latActual, lonActual), 10.0, "#760316");
		}
		for (int i = 0; i < listaVertices.size(); i++) {
			Vertex actual = listaVertices.get(i);
			String latLonActual[] = actual.info().split(";;;");
			double latActual = Double.parseDouble(latLonActual[0]);
			double lonActual = Double.parseDouble(latLonActual[1]);
			for(Edge edge : actual.edges()){
				if(kosaraju.stronglyConnected(actual.index(), edge.ladoTo())){
					contador++;
					String latLonSecond[] = edge.to().split(";;;");
					double latSecond = Double.parseDouble(latLonSecond[0]);
					double lonSecond = Double.parseDouble(latLonSecond[1]);
					mapa.generateSimplePath(new LatLng(latActual, lonActual), new LatLng(latSecond, lonSecond), false, "#870C00");
				}
			}
		}
		System.out.println("la cantidad de arcos de la componente conexa m�s grande es " + contador);
	}



	public void componentesConexas3AMaps() throws IOException{
		String colores3[] = {"#C80600" ,"#FF6800" ,"#FFA200" ,"#FFDC00" ,"#D9FF00" ,
				"#80FF00" ,"#00FFB4" ,"#00FFEA" ,"#00C7FF" ,"#0082FF" ,"#9800FF" ,"#F400FF" , "#FF003A"};

		LinkedList<String> paletaColores = new LinkedList<>();
		for (String string : colores3) {
			paletaColores.add(string);
		}
		int masCongestionado = masCongestionado().degree();
		KosarajuSharirSCC kosaraju = new KosarajuSharirSCC(principalGraph);
		GroupOfVertexConeccted[] group = new GroupOfVertexConeccted[kosaraju.count()];
		////////////////////////////PINTAR VERTICES/////////////////////////////////////////
		for (int i = 0; i < group.length; i++) {
			int num = (int) (Math.random()*paletaColores.size());
			String color = paletaColores.get(num);
			group[i] = new GroupOfVertexConeccted(i, color);
			paletaColores.delete(color);
		}

		int[] componentes = kosaraju.getStronglyConectedIds();
		Vertex[] vertices = principalGraph.indexedArray();
		for (int i = 0; i < vertices.length; i++) {
			Vertex temp = vertices[i];
			int index = componentes[i];
			group[index].addVertex(temp);
		}
		Mapa map = new Mapa("R3 # Vamos ganando ");
		try {
			TimeUnit.SECONDS.sleep(2);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (GroupOfVertexConeccted gvc : group) {
			LinkedList<Vertex> listaVertices1 = gvc.getListaVertices();
			for (int i = 0; i < listaVertices1.size(); i++) {
				Vertex actual = listaVertices1.get(i);
				String latLonActual[] = actual.info().split(";;;");
				double latActual = Double.parseDouble(latLonActual[0]);
				double lonActual = Double.parseDouble(latLonActual[1]);
				double radio = (actual.degree()*150)/(masCongestionado);
				map.generateArea(new LatLng(latActual, lonActual), radio, gvc.getHexColor());
			}
		}
		////////////////PINTAR ARCOS/////////////////////////////////////////////////////////
		for (GroupOfVertexConeccted grupo : group) {
			LinkedList<Vertex> listaVertices = grupo.getListaVertices();
			for (int i = 0; i < listaVertices.size(); i++) {
				Vertex actual = listaVertices.get(i);
				String latLonActual[] = actual.info().split(";;;");
				double latActual = Double.parseDouble(latLonActual[0]);
				double lonActual = Double.parseDouble(latLonActual[1]);
				for(Edge edge : actual.edges()){
					if(kosaraju.stronglyConnected(actual.index(), edge.ladoTo())){
						String latLonSecond[] = edge.to().split(";;;");
						double latSecond = Double.parseDouble(latLonSecond[0]);
						double lonSecond = Double.parseDouble(latLonSecond[1]);
						map.generateSimplePath(new LatLng(latActual, lonActual), new LatLng(latSecond, lonSecond), false, grupo.getHexColor());
					}
				}
			}
		}


	}




	@Override public Camino pathBetween(double lat1, double long1, double lat2, double long2) {
		Vertex v1 = principalGraph.closestVertexTo(lat1, long1), v2 = principalGraph.closestVertexTo(lat2, long2);
		Camino c = new Camino();
		if(v1 != null && v2 != null) {	Dijkstra d = new Dijkstra(principalGraph,v1.index());
		c.agregarArcos(d.pathTo(v2.index())); }

		return c;}

	@Override public Camino[] pathsBetween(double lat1, double long1, double lat2, double long2) {
		Vertex v1 = principalGraph.closestVertexTo(lat1, long1);
		Vertex v2 = principalGraph.closestVertexTo(lat2, long2);
		Camino[] caminos = new Camino[2];
		if(v1 != null && v2 != null) {
			Dijkstra d = new Dijkstra(principalGraph,v1.index());
			Dijkstra dret = new Dijkstra(principalGraph, v2.index());
			caminos[0] = d.path(v2.index());
			caminos[1] = dret.path(v1.index());
		}
		return caminos;
	}

	@Override public Iterable<Camino> patsWhitoutTollsBetween(double lat1, double long1, double lat2, double long2) {

		Vertex vertice1 = principalGraph.closestVertexTo(lat1, long1), vertice2 = principalGraph.closestVertexTo(lat2, long2);
		QueStack<Camino> caminos = new QueStack<>();  RedBlackBST<Camino> caminosToRet = new RedBlackBST<>();

		if(vertice1 != null && vertice2 != null) {
			int v1= vertice1.index(), v2 = vertice2.index();
			Dijkstra dj = new Dijkstra(principalGraph, v1);
			Camino temp = dj.path(v2);
			if(temp != null) {
				if(!temp.hasTolls())caminosToRet.put(temp);
				for (Vertex v : graph) {
					if(v.index() == v2)continue;
					Camino c = dj.path(v.index());
					if(c != null && c.esCamino())caminos.enqueue(c);}

				for (Camino c : caminos) {	Vertex v = c.getVertex();

				if(c.esCamino() && !c.hasTolls() && v.isVertex()) {
					Dijkstra d2 = new Dijkstra(principalGraph, v.index());
					c.unirCaminos(d2.path(v2));
					caminosToRet.put(c);
				}}
			}}
		return caminosToRet.values();
	}

	private void loadCsv() {

		boolean seguir = true; Scanner entrada = null;
		try {entrada = new Scanner(new File("./data/chicagoStreets.csv"));}
		catch (Exception fnE) {System.out.println(fnE.getMessage());seguir = false;}

		if (seguir) while (entrada.hasNext()) {
			String[] coords = entrada.nextLine().split(";");
			for (String s : coords)	if(s.startsWith("-8")) calles.enqueue(s);
		}
	}

	public DigrafoConPeso getDigrafo(){return principalGraph;}

	public void printGraph() {
		for (Vertex v : principalGraph.indexedArray()) 
			for (Edge e : v.edges()) 
				System.out.println(e.ladoFrom()+"-"+e.ladoTo()+"\t --"+e.from()+"-"+e.to());}

	public void printGraphVertex() {
		for (Vertex v : principalGraph.indexedArray()) 
			v.print();}

	public String shuffleCoords() {
		Random r = new Random();
		int i = r.nextInt(calles.size()), count = 0;
		String toRet = new String();
		for (String s : calles)
			if(count >= i) {toRet = s;break;}
			else count++;
		i=r.nextInt(calles.size()); count = 0;
		for (String s : calles)
			if(count >= i) {toRet += "\n"+s;break;}
			else count++;
		toRet = toRet.replace(" ", ",");
		return toRet;}
}
