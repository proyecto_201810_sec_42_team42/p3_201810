package test;

import junit.framework.TestCase;
import structures.data_structures.QueStack;

public class testQueStack extends TestCase{


	private QueStack<String> queStack;

	private void setupEscenario1(){

		queStack = new QueStack<String>();
		queStack.enqueue("Sebastian");
		queStack.enqueue("Andres");
		queStack.enqueue("Alvaro");
		queStack.enqueue("Martha");
		queStack.enqueue("Danilo");
	}

	public void testQueue(){
		setupEscenario1();
		assertEquals(5, queStack.size());
		assertEquals("Danilo", queStack.pop());
		assertEquals(4, queStack.size());
		assertEquals("Sebastian", queStack.dequeue());
	}

	public void testSize(){
		setupEscenario1();
		assertEquals(5, queStack.size());
		queStack.enqueue("Sebastian");
		queStack.enqueue("Andres");
		queStack.enqueue("Alvaro");
		queStack.enqueue("Martha");
		queStack.enqueue("Danilo");
		assertEquals(10, queStack.size());
	}

	public void testDequeue(){
		setupEscenario1();
		assertEquals("Sebastian", queStack.dequeue());
		assertEquals("Andres", queStack.dequeue());
		assertEquals("Alvaro", queStack.dequeue());
		assertEquals("Martha", queStack.dequeue());
		assertEquals("Danilo", queStack.dequeue());
		assertEquals(0, queStack.size());
	}

	public void testPop(){
		setupEscenario1();
		assertEquals("Danilo", queStack.pop());
		assertEquals("Martha", queStack.pop());
		assertEquals("Alvaro", queStack.pop());
		assertEquals("Andres", queStack.pop());
		assertEquals("Sebastian", queStack.pop());


	}


}