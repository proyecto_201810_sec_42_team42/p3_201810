package test;

import junit.framework.TestCase;
import structures.data_structures.Edge;
import structures.data_structures.Vertex;

public class testVertex extends TestCase{


	private Vertex vertex1;
	private Vertex vertex2;
	private Vertex vertex3;
	private Vertex vertex4;


	private void setupEscenario1(){
		vertex1 = new Vertex("10;;;20",0);
		vertex2 = new Vertex("20;;;30",1);
		vertex3 = new Vertex("10;;;20",2);
		vertex4 = new Vertex("20;;;10",3);
	}

	public void testInfo(){
		setupEscenario1();
		assertEquals("10;;;20", vertex1.info());
	}

	public void testDegree(){
		setupEscenario1();
		assertEquals(0, vertex1.degree());
		vertex1.addEdge(new Edge("10", "30", 0, 2));
		vertex1.addEdge(new Edge("10", "40", 0, 3));
		vertex1.addEdge(new Edge("10", "50", 0, 4));
		vertex1.addEdge(new Edge("10", "60", 0, 5));
		assertEquals(4, vertex1.degree());
	}

	public void testEquals(){
		setupEscenario1();
		assertEquals(0, vertex1.compareTo(vertex3));
	}

	public void testEdgesIterable(){
		setupEscenario1();
		vertex1.addEdge(new Edge("10", "30", 0, 2));
		vertex1.addEdge(new Edge("10", "40", 0, 3));
		vertex1.addEdge(new Edge("10", "50", 0, 4));
		vertex1.addEdge(new Edge("10", "60", 0, 5));
		Iterable<Edge> iterar =vertex1.edges();
		for (Edge edge : iterar) {
			System.out.println(edge.from()+ " " + edge.to());
		}
	}
}
