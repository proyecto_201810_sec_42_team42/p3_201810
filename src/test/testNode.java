package test;

import junit.framework.TestCase;
import structures.data_structures.NodeDL;

public class testNode extends TestCase{
	
	private NodeDL<String> node1;
	private NodeDL<String> node2;
	private NodeDL<String> node3;
	private NodeDL<String> node4;

	
	public void setupEscenario(){
		node1 = new NodeDL<String>("sebastian");
		node2= new NodeDL<String>("danilo");
		node3= new NodeDL<String>("diaz");
		node4 = new NodeDL<String>("mujica");
		node1.setNext(node2); 
		node2.setNext(node3);
		node3.setNext(node4);
		node4.setPrev(node3);
		node3.setPrev(node2);
		node2.setPrev(node1);
		
	}
	
	public void testPrevious(){
		setupEscenario();
		assertEquals("diaz", node4.previous().getItem());
		assertNull(node1.previous());
	}
	
	public void testNext(){
		setupEscenario();
		assertEquals("diaz", node2.next().getItem());
		assertEquals(null, node4.next());
	}
	
	public void testValue(){
		setupEscenario();
		assertEquals("sebastian", node1.getItem());
	}
	
	public void getPos(){
		setupEscenario();
		assertEquals("mujica", node1.get(4));
		assertEquals("sebastian", node1.get(0));
		assertEquals(null, node1.get(-1));
	}

}
