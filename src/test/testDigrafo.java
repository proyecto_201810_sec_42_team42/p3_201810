package test;

import junit.framework.TestCase;
import structures.data_structures.DigrafoConPeso;
import structures.data_structures.Edge;
import structures.data_structures.Vertex;

public class testDigrafo extends TestCase{


	private DigrafoConPeso digrafo;

	private void setupEscenario(){
		digrafo = new DigrafoConPeso();
	}

	private void setupEscenario2(){
		digrafo = new DigrafoConPeso();
		digrafo.addVertice("10;;;20");
		digrafo.addVertice("20;;;30");
		digrafo.addVertice("40;;;20");
		digrafo.addVertice("50;;;20");
		digrafo.addVertice("60;;;20");
	}

	private void setupEscenario3(){
		digrafo = new DigrafoConPeso();
		digrafo.addVertice("10;;;20");
		digrafo.addVertice("20;;;30");
		digrafo.addVertice("40;;;20");
		digrafo.addVertice("50;;;20");
		digrafo.addVertice("60;;;20");
		digrafo.addEdge(new Edge("10", "30", 0, 2));
		digrafo.addEdge(new Edge("20", "40", 1, 3));
		digrafo.addEdge(new Edge("60", "50", 5, 4));

	}

	public void testIsEmpty(){
		setupEscenario();
		assertEquals(true, digrafo.isEmpty());
	}

	public void testIsNotEmpty(){
		setupEscenario();
	}

	public void testVertices(){
		setupEscenario2();
		assertEquals(5, digrafo.vertices());
	}

	public void testEdges(){
		setupEscenario3();
		assertEquals(3, digrafo.edges());
	}

	public void testAdjacentList(){
		setupEscenario3();
		Iterable<Edge> iter = digrafo.adyacentesA("20;;;30");
		for (Edge edge : iter) {
			System.out.println(edge.from() + ";;;" + edge.to());
		}
	}

	public void testAllVertex(){
		setupEscenario2();
		Iterable<Vertex> iter = digrafo.allVertices();
		System.out.println("//////////////////////////////////////");
		System.out.println("public void testAllVertex() \n");
		for (Vertex vertex : iter) {
			System.out.println(vertex.info());
		}
		System.out.println("//////////////////////////////////////");

	}

}
