package test;

import junit.framework.TestCase;
import structures.data_structures.Edge;

public class testEdge extends TestCase{

	private Edge arco1;
	private Edge arco2;

	private void setupEscenario(){

		arco1 = new Edge("10", "20", 0, 1);
		arco2 = new Edge("20", "10", 1, 0);
	}

	public void testLadoInicioFin(){
		setupEscenario();
		assertEquals("10", arco1.from());
		assertEquals("20", arco1.to());
	}

	public void testEquals(){
		setupEscenario();
		Edge temp = new Edge("10", "20", 0, 1);
		assertEquals(true, temp.equals(arco1));
		assertEquals(false, arco1.equals(arco2));
	}


	public void testWeight(){
		setupEscenario();
	}

}
