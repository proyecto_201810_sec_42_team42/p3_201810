package structures.data_structures;

import java.io.*;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

public class DigrafoConPeso {

	private int edges;
	/**
	 * lista de adjacencia con un vertice y lista de enlaces en cada espacio del arreglo
	 */
	private SeparateChainingHashST<String, Vertex> MyAdjacentList;
	/**
	 * crea un nuevo D�grafo con espacio para <b>capacity</b> vertices
	 * @param capacity cantidad de espacios para la isersi�n de v�rtices
	 */
	public DigrafoConPeso(int capacity) {MyAdjacentList = new SeparateChainingHashST<>(capacity);}
	public DigrafoConPeso() {this(350);}

	/**
	 * For this constructor the format for the archive is: <b>siseOfTheGraph whit distance bettwen vertices and cantity of vertices separated by "-"</b>
	 * @param ruta path of the archive to create a new graph
	 */
	public DigrafoConPeso(String ruta) {
		String[] vertices = ruta.split("-");	int capacity = Integer.parseInt(vertices[1]);
		MyAdjacentList = new SeparateChainingHashST<>(capacity);
		Gson g = new GsonBuilder().create();
		try {JsonReader r = new JsonReader(new InputStreamReader(new FileInputStream(new File(ruta)), "UTF-8"));
		r.beginArray();	
		while (r.hasNext()) {
			Edge e = g.fromJson(r, Edge.class);addEdge(e);}
		} catch (Exception e) {System.err.println("Error en la carga del d�grafo a partir del archivo: " + e.getMessage()); e.printStackTrace();}
	}

	public int edges() 		 {return edges;						}
	public int vertices() 	 {return MyAdjacentList.size();		}
	public boolean isEmpty() {return MyAdjacentList.size() == 0;}

	public void addVertice(String infoVertice) {
		if(!MyAdjacentList.contains(infoVertice))
			MyAdjacentList.put(infoVertice, new Vertex(infoVertice, MyAdjacentList.size()));
	}
	public String getInfoVertice(String infoVertice) {
		Vertex temp = MyAdjacentList.get(infoVertice);return (temp != null)?temp.info():"NaN";}

	/**
	 * a�ade un vertice ya creado
	 */
	public void addEdge(Edge newEdge) {
		if(!MyAdjacentList.contains(newEdge.from())) 
			MyAdjacentList.put(newEdge.from(), new Vertex(newEdge.from(), newEdge.ladoFrom()));

		if(!MyAdjacentList.contains(newEdge.to())) 
			MyAdjacentList.put(newEdge.to(), new Vertex(newEdge.to(), newEdge.ladoTo()));
		MyAdjacentList.get(newEdge.from()).addEdge(newEdge); edges++;
		MyAdjacentList.get(newEdge.to()).addEdgeIn(newEdge);
	}

	public Vertex[] indexedArray() {
		Vertex[] toRet = new Vertex[vertices()];
		for (Vertex v : MyAdjacentList.vals())
			toRet[v.index()] = v;
		return toRet;
	}

	public Vertex closestVertexTo(double lat1, double long1) {
		Vertex toRet = null;
		double minimunDistance = Double.POSITIVE_INFINITY;
		for (Vertex v : MyAdjacentList.vals()) 
			if(v.distanciaCoord(v.info(), lat1, long1) < minimunDistance) {
				toRet = v;
				minimunDistance = v.distanciaCoord(v.info(), lat1, long1);
			}
		return toRet;
	}

	public Iterable<Edge> adyacentesA(String vertice) {return MyAdjacentList.get(vertice).edges();}
	public Iterable<Edge> entrantesA(String vertice) {return MyAdjacentList.get(vertice).edgesIn();}
	public Iterable<Vertex> allVertices(){return this.MyAdjacentList.vals();}
	public Iterable<Edge> allEdges(){
		QueStack<Edge> toRet = new QueStack<>();
		for (Vertex v : MyAdjacentList.vals()) 
			for (Edge e : v.edges()) 
				toRet.enqueue(e);
		return toRet;
	}


	//TODO : fix this shit.
	public Iterable<Edge> adj(int v) {
		//		return indexedArray()[v].edges();
		return null;
	}


	public DigrafoConPeso reverse() {
		// TODO Auto-generated method stub
		DigrafoConPeso reverse = new DigrafoConPeso(vertices());
		for (int v = 0; v < vertices(); v++) {
			for (Edge w : indexedArray()[v].edges()) {
				reverse.addEdge(new Edge(w.to(), w.from(), w.ladoTo(), w.ladoFrom()));
			}
		}
		return reverse;
	}



}
