package structures.data_structures;

public class Vertex implements Comparable<Vertex>{

	/**
	 * Index del v�rtice como arreglo
	 */
	private int index;
	/**
	 * Strign composed by the latitude and the longitude separated by <b><i>;;;</i></b>
	 */
	private String info;
	/**
	 * edges from this vertex to another
	 */
	private RedBlackBST<Edge> adj;
	/**
	 * edges another this vertex to this
	 */
	private RedBlackBST<Edge> edgesIn;

	/**
	 * start a new vertex whit ID = "NaN"
	 */
	public Vertex() {this("NaN", -1);}

	public Vertex(String vertexInfo, int index) {this.info = vertexInfo;
	this.index = index;
	edgesIn = new RedBlackBST<>();
	adj = new RedBlackBST<>();}

	/**
	 * 
	 * @return the composed String of the latitude;;;longitude
	 */
	public String info() {return this.info;}

	public void setInfo(String newInfo) {this.info = newInfo;}

	public int index() {return this.index;}

	public int degree() {return this.adj.size();}

	public boolean isVertex() {return this.info != "NaN";}

	/**
	 * @param newEdge edge to find a existent edge
	 * @return true if exist an edge between whit the initial conditions <br> <i>initial conditions: the same initial vertex, the same destiny</i>
	 */
	public boolean addEdge(Edge newEdge) {
		Edge temp = adj.get(newEdge);

		if   (temp != null) temp.sumWeight(newEdge);
		else if(temp == null) adj.put(newEdge);
		return temp != null;
	}

	public void addEdgeIn(Edge newEdge) {if(!edgesIn.contains(newEdge)) edgesIn.put(newEdge);}

	public double distanciaCoord(double lat1, double long1,double lat2, double long2) {  
		double earthRadius = 6371000;
		lat1 = Math.toRadians(lat1);	lat2 = Math.toRadians(lat2);	long1 = Math.toRadians(long1);	long2 = Math.toRadians(long2);
		double dlon = (long2 - long1),  dlat = (lat2 - lat1), sinlat = Math.sin(dlat / 2), sinlon = Math.sin(dlon / 2);
		double c = 2 * Math.asin (Math.min(1.0, Math.sqrt((sinlat * sinlat) + Math.cos(lat1)*Math.cos(lat2)*(sinlon*sinlon))));
		return 	earthRadius * c;}  

	public double distanciaCoord(String formatedPoint,double lat2, double long2) {
		String[] la1lo2= formatedPoint.split(";;;");
		double lat1 = Double.parseDouble(la1lo2[0]);
		double long1 = Double.parseDouble(la1lo2[1]);
		return distanciaCoord(lat1, long1, lat2, long2);
	} 

	public Iterable<Edge> edges(){return this.adj.values();}

	public Iterable<Edge> edgesIn(){return this.edgesIn.values();}

	public boolean equals(String info) {return this.info.equals(info);}

	public Edge getEdge(Vertex To) {for (Edge e : adj.values())	if(e.to().equals(To.info))return e;
	return null;}

	@Override public int compareTo(Vertex o) {return Integer.compare((this.degree()+this.edgesIn.size()), (o.degree()+o.edgesIn.size()));}

	public void print(){
		System.out.println("\nLatitud: " + this.info.split(";;;")[0] + ". Longitud: "+this.info.split(";;;")[1]);
		System.out.println("Total servicios que salieron: " + this.degree() + ". Total servicios que llegaron: "+this.edgesIn.size());

		System.out.println("___________________________________");;
	}
	
	public int edgesInSize(){
		return edgesIn.size();
	}

}
