package structures.data_structures;

public class Edge implements Comparable<Edge>{

	private double total;
	private int cantServices, lFrom, lTo, srvcWToll, totalSeconds;
	private double totalMiles;
	private String ladoIn, ladoFn;

	public Edge(String Inicio, String Fin, int ladoFrom, int ladoTo) {
		this.ladoIn = Inicio;
		this.ladoFn  = Fin ;
		this.lFrom = ladoFrom;
		this.lTo = ladoTo;
		total = totalMiles = srvcWToll = cantServices = totalSeconds = 0;
	}

	public String from() 	{return ladoIn;	}
	public String to()  	{return ladoFn;	}
	public int ladoFrom() 	{return lFrom;	}
	public int ladoTo() 	{return lTo;	}
	public double total () 	{return total;	}
	public int time () {return totalSeconds;}
	public double miles(){return totalMiles;}
	public double weight() 	{return (total + totalMiles + totalSeconds)/(cantServices -srvcWToll);}

	public void sumWeight(Edge weightToAdd) {
		this.total 		  += weightToAdd.total		 ;
		this.totalMiles   += weightToAdd.totalMiles	 ;
		this.srvcWToll 	  += weightToAdd.srvcWToll	 ;
		this.cantServices += weightToAdd.cantServices;
		this.totalSeconds += weightToAdd.totalSeconds;
	}

	/**
	 * @return true if the current edge have tolls
	 */
	public boolean hasTolls() {return srvcWToll != 0;}
	public String coordIn() {
		String [] a = ladoIn.split(";;;");
		return a[1] + "," + a[0];
	}
	public String coordFn() {
		String [] a = ladoFn.split(";;;");
		return a[1] + "," + a[0];
	}

	public boolean equals(Edge e) {return this.compareTo(e) ==0;}
	@Override public int compareTo(Edge e) {return (this.ladoIn +";;;"+this.ladoFn).compareTo((e.ladoIn+";;;"+e.ladoFn));}
	@Override public String toString(){ return String.format("%d-%d %.2f %.2f", lFrom, lTo, total, totalMiles);}}
