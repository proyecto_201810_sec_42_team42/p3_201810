package structures.data_structures;
import java.util.Iterator;

/**
 * This ADT should contain the basic operations to manage a list
 * add: add a new element T 
 * delete: delete the given element T 
 * get: get the given element T (null if it doesn't exist in the list)
 * size: return the the number of elements
 * get: get an element T by position (the first position has the value 0) 
 * listing: set the listing of elements at the firt element
 * getCurrent: return the current element T in the listing (return null if it doesn�t exists)
 * next: advance to next element in the listing (return if it exists)
 * @param <T>
 */
public class LinkedList <T extends Comparable<T>> implements IList<T> 
{
	private Node primero;

	private int tamanio;

	private Node actual;
	
	private Node ultimo;

	public LinkedList(){
		primero = null;
		tamanio=0;
		actual=null;
		ultimo =null;
	}
	
	public void reset(){
		primero = null;
		tamanio=0;
		actual=null;
		ultimo =null;
	}

	public void add(T pElement) {
		// TODO Auto-generated method stub
		Node elemento = new Node(pElement);
		if(primero==null){
			primero= elemento;
			ultimo = primero;
			tamanio++;
		}
		else if(primero!=null)
		{
			ultimo.cambiarSiguiente(elemento);
			ultimo.darSiguiente().cambiarAnterior(ultimo);
			ultimo = ultimo.darSiguiente();
			tamanio++;
		}


	}

	public void addNoRepetido(T pElement){
		Node elemento = new Node(pElement);
		if(primero==null){
			primero= elemento;
			tamanio++;
		}
		else if(primero!=null && primero.darElemento().compareTo(pElement)!=0){
			Node actual = primero;
			boolean centinela= true;
			while(actual.darSiguiente()!=null)
			{
				if(actual.darElemento().compareTo(pElement)==0)
				{
					centinela= false;
					break;
				}
				actual= actual.darSiguiente();
			}
			if(centinela){
				Node ultimo = actual;
				ultimo.cambiarSiguiente(elemento);
				ultimo.darSiguiente().cambiarAnterior(ultimo);
				tamanio++;
			}
		}
	}

	/**
	 * metodo auxiliar que ayudara a volver
	 * al primer elemento de una lista
	 */
	public void resetIterator() 
	{
		actual = primero;
	}

	public void delete(T pElement) {
		// TODO Auto-generated method stub
		Node buscado=primero;
		if(buscado.darElemento().compareTo(pElement)==0){
			primero=primero.darSiguiente();
			tamanio--;
		}
		else if(ultimo.darElemento().compareTo(pElement)==0)
		{
			Node anterior= ultimo.darAnterior();
			ultimo = anterior;
			anterior.cambiarSiguiente(null);
			tamanio--;
		}
		else{
			while(buscado.darElemento().compareTo(pElement)!=0 && buscado.darSiguiente()!=null){
				buscado= buscado.darSiguiente();
			}
			if(buscado.darElemento().compareTo(pElement)==0)
			{
				Node anterior = buscado.darAnterior();
				Node siguiente = buscado.darSiguiente();
				anterior.cambiarSiguiente(siguiente);
				siguiente.cambiarAnterior(anterior);
				tamanio--;
			}
		}
	}

	public T get(T pElement) {
		// TODO Auto-generated method stub
		Node buscado=primero;
		boolean termino = false;
		while(!termino && buscado!= null)
		{
			if(buscado.darElemento().compareTo(pElement)==0) 
			{
				termino = true;
				return (T) buscado.darElemento();
			}
			else
			{
				buscado= buscado.darSiguiente();
			}
		}
		return null;

	}

	public int size() {
		// TODO Auto-generated method stub
		return tamanio;
	}

	public T get(int pPosition) {
		// TODO Auto-generated method stub
		if(tamanio==0){
			return null;
		}
		int contador = 1;
		Node actual = primero;
		while(contador!=pPosition && actual.darSiguiente()!=null){
			actual = actual.darSiguiente();
			contador++;
		}
		return actual.darElemento();
	}

	public Node Listing() {
		// TODO Auto-generated method stub
		if(primero!= null)
		{
			return primero;
		}
		else
			return null;
	}

	public T getCurrent() {
		if(primero!= null){
			return actual.darElemento();
		}
		else
			return null;
	}

	public T next() {
		if(actual== null)
		{
			actual= primero;
		}
		else if(!hasNext()){
			return null;
		}
		else
			actual = actual.darSiguiente();
		return actual.darElemento();
	}

	public boolean hasNext(){
		boolean rta =false;
		if(actual.darSiguiente()!=null)
			return true;
		return rta;
	}


	public Node buscarUltimo()
	{
		Node actual = primero;
		boolean termino = false;
		while(!termino)
		{
			if( actual.darSiguiente() == null)
			{
				termino = true;
			}
			else
			{
				actual = actual.darSiguiente();
			}
		}
		return actual;
	}


	public class Node  {

		private T elemento;

		private Node siguiente;

		private Node anterior;

		public Node(T t){
			elemento = t;
		}

		public Node darSiguiente(){
			return siguiente;
		}

		public Node darAnterior(){
			return anterior;
		}

		public T darElemento(){
			return elemento;
		}

		public void cambiarSiguiente(Node pNode){
			this.siguiente= pNode;
		}

		public void cambiarAnterior(Node pAnterior){
			this.anterior = pAnterior;
		}
	}


	@Override
	public Iterator<T> iterator() {
		return null;
	}

}
